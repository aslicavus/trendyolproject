package com.trendyol.pages;

import com.trendyol.utils.BasePage;
import com.trendyol.utils.MenuItems;
import com.trendyol.utils.TrendyolUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Categories extends BasePage {

    public Categories(WebDriver driver) {
        super(driver);
    }

    public void browseAllCategories(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

//        openCategory(MenuItems.KADIN);
        openCategory(MenuItems.ERKEK);
        openCategory(MenuItems.COCUK);
        openCategory(MenuItems.AYAKKABI_CANTA);
        openCategory(MenuItems.SAAT_AKSESUAR);
        openCategory(MenuItems.KOZMETIK);
        openCategory(MenuItems.EV_YASAM);
        openCategory(MenuItems.ELEKTRONIK);
        openCategory(MenuItems.SUPERMARKET);
    }

    public void randomCategories(){
        Map<Integer, MenuItems> menuItemsMap = new HashMap<Integer, MenuItems>();
        menuItemsMap.put(0, MenuItems.KADIN);
        menuItemsMap.put(1, MenuItems.ERKEK);
        menuItemsMap.put(2, MenuItems.COCUK);
        menuItemsMap.put(3, MenuItems.AYAKKABI_CANTA);
        menuItemsMap.put(4, MenuItems.ELEKTRONIK);
        menuItemsMap.put(5, MenuItems.SUPERMARKET);
        menuItemsMap.put(6, MenuItems.KOZMETIK);
        menuItemsMap.put(7, MenuItems.EV_YASAM);
        menuItemsMap.put(8, MenuItems.SAAT_AKSESUAR);

        Random rand = new Random();
        Integer randomCategoryNum = rand.nextInt(menuItemsMap.size());

        openCategory(menuItemsMap.get(randomCategoryNum));
        openFirstItem();

    }

    public void addToCartOpenItem(){
        try {
            if(TrendyolUtils.isLogin(driver)){
                if(isItemPage()){
                    driver.findElement(By.xpath("//*[@id=\"product-detail-app\"]/div[1]/div[2]/div[2]/div[2]/div[2]/button[1]/div[1]")).click();
                    log.info("Urun sepete eklendi");
                }
            } else {
                Login login = new Login(driver);
                login.login();
                Thread.sleep(3000);
                randomCategories();
                Thread.sleep(3000);
                if(isItemPage()){
                    boolean bodySize = false;
                    try {
                        driver.findElement(By.xpath("//html[@id='ng-app']//div[@id='product-detail-app']/div[@class='pd-app-container']/div[@class='pr-cn']//div[@class='pr-in-sz-pk']"));
                        bodySize = true;
                    } catch (Exception e){
                        bodySize = false;
                    }
                    if(bodySize){
                        driver.findElement(By.xpath("//html[@id='ng-app']//div[@id='product-detail-app']/div[@class='pd-app-container']/div[@class='pr-cn']//div[@class='pr-in-sz-pk']")).click();
                        driver.findElement(By.xpath("//html[@id='ng-app']//div[@id='product-detail-app']/div[@class='pd-app-container']/div[@class='pr-cn']//ul[@class='pr-in-drp-u']/li[1]")).click();
                        Thread.sleep(3000);
                    }
                    driver.findElement(By.xpath("//html[@id='ng-app']//div[@id='product-detail-app']/div[@class='pd-app-container']/div[@class='pr-cn']//div[@class='add-to-bs-tx']")).click();
                    log.info("Urun sepete eklendi");
                }
            }
        }catch (Exception ex){
            System.err.println(ex);
            log.info("Urun sepete eklenirken hata olustu");
        }
    }

    private boolean isItemPage(){
        try {
            driver.findElement(By.xpath("//html[@id='ng-app']//div[@id='product-detail-app']/div[@class='pd-app-container']/div[@class='pr-cn']//div[@class='add-to-bs-tx']"));
            return true;
        } catch (Exception e){
            return false;
        }
    }

    private boolean openFirstItem(){
        try {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.findElement(By.xpath("/html//div[@id='browsing-gw-homepage']//div[@class='sticky-wrapper']/div[1]")).click();
//            driver.findElement(By.xpath("//*[@id=\"browsing-gw-homepage\"]/div[1]/div[1]/div[1]/div[1]/article[1]/a[1]/span[1]/img[1]")).click();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.findElement(By.xpath("//html[@id='ng-app']//div[@id='root']/div/ul/li[1]")).click();
//            driver.findElement(By.xpath("//*[@id=\"root\"]/div[1]/ul[1]/li[1]")).click();

            log.info("Urun detay sayfasi acildi");
            return true;

        }catch (Exception ex){
            System.err.println(ex);
            log.info("Urun detay sayfasi acilirken hata olsutu");
        }
        return false;
    }

    private void openCategory(MenuItems menuItems){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebDriverWait webDriverWait = new WebDriverWait(driver, 30);
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(menuItems.getXpath())));
        WebElement webElement=driver.findElement(By.xpath(menuItems.getXpath()));
        webElement.click();
        List<WebElement> list = driver.findElements(By.tagName("img"));
        int invalidImageNum = 0;
        for(WebElement element : list){
            if(!TrendyolUtils.verifyImageActive(element)){
                invalidImageNum++;
            }
        }
        System.out.println(menuItems.toString() + " kategorisi açıldı..");
        log.info(menuItems.toString() + " kategorisi açıldı");
        log.info(menuItems.toString() + " kategorisi yuklenemeyen resim sayisi : " + invalidImageNum);
    }
}
