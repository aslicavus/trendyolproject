package com.trendyol.pages;

import com.trendyol.utils.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage extends BasePage {

    public HomePage(WebDriver driver){
        super(driver);
    }

    public void openHomePage(){
        try {
            log.info("Anasayfa aciliyor");
            driver.get("https://www.trendyol.com/");
            log.info("Anasayfa acildi");

            driver.manage().window().maximize();
            log.info("Tarayici tam ekran yapildi");

            try {
                WebElement pop_up = driver.findElement(By.className("homepage-popup-img"));
                pop_up.click();
                log.info("Anasayfa popup kapatildi");
            } catch (Exception e){
                System.err.println(e);
                log.info("Anasayfa acilirken hata olsutu");
            }
        }catch (Exception ex){
            System.err.println(ex);
        }
    }

}
