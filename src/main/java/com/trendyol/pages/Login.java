package com.trendyol.pages;

import com.trendyol.utils.BasePage;
import com.trendyol.utils.TrendyolUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Login extends BasePage {

    public Login(WebDriver driver) {
        super(driver);
    }

    public void login(){
        try{
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

            if(!TrendyolUtils.isLogin(driver)){
                WebDriverWait webDriverWait = new WebDriverWait(driver, 30);
                webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.className("login-register-button-container")));
                WebElement webElement=driver.findElement(By.className("login-register-button-container"));
                webElement.click();
                Thread.sleep(1000);

                Properties prop = getProp();

                driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys(prop.getProperty("trendyol.test.username"));

                driver.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys(prop.getProperty("trendyol.test.password"));

                driver.findElement(By.xpath("//*[@id=\"loginSubmit\"]")).click();

                log.info("Giris yapildi");
            } else {
                log.info("Oturum zaten acik");
            }


        } catch (Exception ex){
            System.err.println(ex);
            log.info("Oturum acilirken hata olustu");
        }
    }

}
