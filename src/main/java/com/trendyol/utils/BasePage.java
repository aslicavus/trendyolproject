package com.trendyol.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class BasePage {

    private static final int TIMEOUT = 5;
    private static final int POLLING = 100;

    protected WebDriver driver;
    private WebDriverWait wait;
    private Properties prop;
    public Logger log;

    public BasePage(WebDriver driver) {
        log = Logger.getLogger("TrendyolTest");

        this.driver = driver;
        wait = new WebDriverWait(driver, TIMEOUT, POLLING);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, TIMEOUT), this);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

        try {
            InputStream input = new FileInputStream(System.getProperty("user.dir") + "/src/main/resources/config.properties");
            prop = new Properties();
            prop.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
            log.info("Konfigurasyon dosyasi acilirken hata olustu");
        }
    }

    public Properties getProp(){
        return prop;
    }

}
