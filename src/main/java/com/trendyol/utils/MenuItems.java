package com.trendyol.utils;

public enum MenuItems {
    KADIN("//div[@id='navigation-wrapper']//ul[@class='main-nav']//a[@href='/butik/liste/kadin']"),
    COCUK("//*[@id=\"navigation-wrapper\"]/nav[1]/ul[1]/li[3]/a[1]"),
    ERKEK("//*[@id=\"navigation-wrapper\"]/nav[1]/ul[1]/li[2]/a[1]"),
    AYAKKABI_CANTA("//*[@id=\"navigation-wrapper\"]/nav[1]/ul[1]/li[4]/a[1]"),
    SAAT_AKSESUAR("//*[@id=\"navigation-wrapper\"]/nav[1]/ul[1]/li[5]/a[1]"),
    KOZMETIK("//*[@id=\"navigation-wrapper\"]/nav[1]/ul[1]/li[6]/a[1]"),
    EV_YASAM("//*[@id=\"navigation-wrapper\"]/nav[1]/ul[1]/li[7]/a[1]"),
    ELEKTRONIK("//*[@id=\"navigation-wrapper\"]/nav[1]/ul[1]/li[8]/a[1]"),
    SUPERMARKET("//*[@id=\"navigation-wrapper\"]/nav[1]/ul[1]/li[9]/a[1]");

    private String xpath;

    MenuItems(String xpath){
        this.xpath = xpath;
    }

    public String getXpath() {
        return xpath;
    }
}
