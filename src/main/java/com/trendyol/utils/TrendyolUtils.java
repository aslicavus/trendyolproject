package com.trendyol.utils;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TrendyolUtils {

    public static boolean isLogin(WebDriver driver){
        try {
            driver.findElement(By.xpath("//li[@id='accountBtn']/div[@class='icon-container']/a[@href='/Hesabim/#/Siparislerim']"));
            return true;
        } catch (Exception e){
            return false;
        }
    }

    public static boolean verifyImageActive(WebElement imgElement) {
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(imgElement.getAttribute("src"));
            HttpResponse response = client.execute(request);
            if (response.getStatusLine().getStatusCode() != 200)
                return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

}
