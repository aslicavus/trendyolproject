package com.trendyol.pages;

import com.trendyol.utils.BaseTest;
import org.testng.annotations.Test;

public class TrendyolTests extends BaseTest {

    @Test(priority = 1)
    public void openHomePage(){
        HomePage homePage = new HomePage(getDriver());
        homePage.openHomePage();
    }

    @Test(dependsOnMethods = {"openHomePage"}, priority = 3)
    public void browseAllCategories(){
        Categories categories = new Categories(getDriver());
        categories.browseAllCategories();
    }

    @Test(dependsOnMethods = {"openHomePage"}, priority = 2)
    public void login(){
        Login login = new Login(getDriver());
        login.login();
    }

    @Test(dependsOnMethods = {"openHomePage"}, priority = 4)
    public void randomCategories(){
        Categories categories = new Categories(getDriver());
        categories.randomCategories();
    }

    @Test(dependsOnMethods = {"openHomePage"}, priority = 5)
    public void addToCartOpenItem(){
        Categories categories = new Categories(getDriver());
        categories.addToCartOpenItem();
    }

}
